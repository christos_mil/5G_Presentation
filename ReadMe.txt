####################################
# author:   Christos Milarokostas  #
# date:	    09/01/2017             #
####################################

This project was originally an assignment of the course "Mobile and Personal
Communications Systems", that I attended during Winter 2016/17 at the National
and Kapodistrian University of Athens (N.K.U.A.) - Department of Informatics
and Telecommunications.

Version 0.2 additions:
======================
i.   filtered OFDMA
ii.  Beamforming
iii. Network Slicing